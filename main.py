def nombre_voyelles(chaine):
    # Convertir la chaîne en minuscules
    chaine = chaine.lower()

    # Définir une liste de voyelles
    voyelleslist = "àâäéèêëîïôöùûüÿçaeiouy"

    # Initialiser le compteur de voyelles
    resultatvoyelles = 0

    # Parcourir la chaîne de caractères
    for caractere in chaine:
        if caractere in voyelleslist:
            resultatvoyelles += 1

    return resultatvoyelles


# Exemple d'utilisation
chaine = input("Écrire votre phrase ici pour compter  le nombre de voyelle : ")
nombre_de_voyelles = nombre_voyelles(chaine)
print("Le nombre de voyelles dans la chaîne est :", nombre_de_voyelles)
